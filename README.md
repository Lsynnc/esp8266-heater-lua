# Project Overview #

Lua source code for heater, based on esp8266 chip + relay module.

## How it works ##

After start, module tries to reconnect to known WiFi network. If it can't do that in reasonable time, it starts an AP with known name and password, so it can be configured.

## Working Modes ##

### Normal Mode ###

Module works in this mode after normal start, when it's succesfully connected to known WiFi network. Every minute it sends it's presence status to a main datacenter (thingspeak.com, for ex.), then switches to a server mode and gets ready to process commands.

### Waiting for configuration mode ###

If the module can't connect to a known WiFi network in the mean time, it switches to Access Point mode, with a known name and password. In this mode it accepts configuration commands and a "Restart" command, so it can be properly configured to connect to another WiFi network.

From time to time it tries to connect to the known WiFi network, for the reason of reconnect if it become accessible again.


# Readme TODOs #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact